# Aplicação Demonstradora - Team3

## Projeto de Gestão de Inventários
Esta aplicação demonstradora visa simular uma gestão de inventário de material de escritório duma empresa hipotética. 

## Implementação

A aplicação será implementada em C# de acordo com o padrão Model-View-Controller, segundo Curry & Grace. A sua interface será implementada em WinForms, pois é a tecnlogia que mais se adequa ao que nos é pedido neste projeto.

