﻿namespace GestaoInventario
{
    partial class FormAlterar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TxtBox_Marca = new System.Windows.Forms.TextBox();
            this.TxtBox_Modelo = new System.Windows.Forms.TextBox();
            this.Btn_gravar = new System.Windows.Forms.Button();
            this.Btn_Cancelar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // TxtBox_Marca
            // 
            this.TxtBox_Marca.Location = new System.Drawing.Point(72, 23);
            this.TxtBox_Marca.Name = "TxtBox_Marca";
            this.TxtBox_Marca.Size = new System.Drawing.Size(139, 23);
            this.TxtBox_Marca.TabIndex = 0;
            // 
            // TxtBox_Modelo
            // 
            this.TxtBox_Modelo.Location = new System.Drawing.Point(72, 72);
            this.TxtBox_Modelo.Name = "TxtBox_Modelo";
            this.TxtBox_Modelo.Size = new System.Drawing.Size(139, 23);
            this.TxtBox_Modelo.TabIndex = 1;
            // 
            // Btn_gravar
            // 
            this.Btn_gravar.Location = new System.Drawing.Point(26, 142);
            this.Btn_gravar.Name = "Btn_gravar";
            this.Btn_gravar.Size = new System.Drawing.Size(75, 23);
            this.Btn_gravar.TabIndex = 2;
            this.Btn_gravar.Text = "Gravar";
            this.Btn_gravar.UseVisualStyleBackColor = true;
            this.Btn_gravar.Click += new System.EventHandler(this.Btn_gravar_Click);
            // 
            // Btn_Cancelar
            // 
            this.Btn_Cancelar.Location = new System.Drawing.Point(136, 142);
            this.Btn_Cancelar.Name = "Btn_Cancelar";
            this.Btn_Cancelar.Size = new System.Drawing.Size(75, 23);
            this.Btn_Cancelar.TabIndex = 3;
            this.Btn_Cancelar.Text = "Cancelar";
            this.Btn_Cancelar.UseVisualStyleBackColor = true;
            this.Btn_Cancelar.Click += new System.EventHandler(this.Btn_Cancelar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 15);
            this.label1.TabIndex = 4;
            this.label1.Text = "Marca";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 15);
            this.label2.TabIndex = 5;
            this.label2.Text = "Modelo";
            // 
            // FormAlterar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(238, 182);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Btn_Cancelar);
            this.Controls.Add(this.Btn_gravar);
            this.Controls.Add(this.TxtBox_Modelo);
            this.Controls.Add(this.TxtBox_Marca);
            this.Name = "FormAlterar";
            this.Text = "Alterar Item";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private TextBox TxtBox_Marca;
        private TextBox TxtBox_Modelo;
        private Button Btn_gravar;
        private Button Btn_Cancelar;
        private Label label1;
        private Label label2;
    }
}