﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoInventario
{
    public partial class FormAlterar : Form
    {
        public ViewAlterar? View { get; set; }
        public Item? ItemAlterar { get; set; }

        public FormAlterar()
        {
            InitializeComponent();
        }

        public void ExibirResultadoItemAlterado()
        {
            DialogResult resultado = MessageBox.Show("Item alterado com sucesso", "Alterar Item", MessageBoxButtons.OK);

            if (resultado == DialogResult.OK)
                this.Close(); ;
        }

        public void PrepararItem()
        {
            // apenas deixo mudar marca e modelo
            TxtBox_Marca.Text = ItemAlterar?.Marca;
            TxtBox_Modelo.Text = ItemAlterar?.Modelo;
        }

        public void FecharForm()
        {
            TxtBox_Marca.Text = string.Empty;
            TxtBox_Modelo.Text = string.Empty;

            this.Close();
        }

        private void Btn_gravar_Click(object sender, EventArgs e)
        {
            ItemAlterar.Marca = TxtBox_Marca.Text;
            ItemAlterar.Modelo = TxtBox_Modelo.Text;

            View?.AlterarItem(ItemAlterar);
        }

        private void Btn_Cancelar_Click(object sender, EventArgs e)
        {
            View?.ClicouEmCancelar(sender, e);
        }
    }
}
