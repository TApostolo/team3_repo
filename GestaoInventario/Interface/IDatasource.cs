﻿namespace GestaoInventario.Interface
{
    public interface IDatasource
    {
        #region ADD
        /// <summary>
        /// Adiciona um item novo
        /// </summary>
        /// <param name="itemParaAdicionar"></param>
        /// <returns></returns>
        bool AdicionarItem(Item itemParaAdicionar);
        #endregion

        #region GET
        /// <summary>
        /// Lista todo o inventário existente
        /// </summary>
        /// <returns></returns>
        List<Item> ListarInventario();
        /// <summary>
        /// Obtém um dado item
        /// </summary>
        /// <param name="itemID"></param>
        /// <returns></returns>
        Item ObterItem(int itemID);
        #endregion

        #region UPDATE
        /// <summary>
        /// Altera a marca e modelo dum dado item
        /// </summary>
        /// <param name="itemID"></param>
        /// <param name="modelo"></param>
        /// <param name="marca"></param>
        /// <returns></returns>
        bool AlterarItem(int itemID, string modelo, string marca);
        #endregion

        #region DELETE
        /// <summary>
        /// Remove o item indicado
        /// </summary>
        /// <param name="itemID"></param>
        /// <returns></returns>
        bool RemoverItem(int itemID);
        #endregion
    }
}
