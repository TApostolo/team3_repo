﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoInventario
{
    public class ViewAlterar
    {
        private FormAlterar? FormularioAlterarItem;
        private FormMain? Pai;

        // eventos
        public delegate void NotificarUtilizadorClicouEmGravar(Item item);
        public NotificarUtilizadorClicouEmGravar? UtilizadorClicouEmGravar;
        public event EventHandler? UtilizadorClicouEmCancelar;

        public ViewAlterar(FormMain pai)
        {
            Pai = pai;
        }

        public void AtivarInterface(Item item)
        {
            FormularioAlterarItem = new FormAlterar()
            {
                View = this,
                ItemAlterar = item
            };
        }

        public void FecharForm()
        {
            FormularioAlterarItem?.FecharForm();
        }

        public void ExibirFormAlterar(FormMain origem)
        {
            if(Pai==null)
                Pai = origem;
            FormularioAlterarItem?.PrepararItem();
            FormularioAlterarItem?.ShowDialog(Pai);
        }

        public void ExibirResultadoItemAlterado()
        {
            FormularioAlterarItem?.ExibirResultadoItemAlterado();
        }

        public void AlterarItem(Item item)
        {
            UtilizadorClicouEmGravar?.Invoke(item);
        }

        public void ClicouEmCancelar(object sender, EventArgs e)
        {
            UtilizadorClicouEmCancelar?.Invoke(sender, e);
        }

    }
}
