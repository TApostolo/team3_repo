﻿using System.Runtime.Serialization;

namespace GestaoInventario
{
    public class FormularioInexistenteException : Exception
    {
        public FormularioInexistenteException()
        { 
        }

        public FormularioInexistenteException(string message) : base(message)
        {

        }

        public FormularioInexistenteException(string message, Exception innerException) : base(message, innerException)
        {

        }

        protected FormularioInexistenteException(SerializationInfo info, StreamingContext context) : base(info, context)
        {

        }
    }
}
