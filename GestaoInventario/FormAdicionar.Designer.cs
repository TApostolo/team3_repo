﻿namespace GestaoInventario
{
    partial class FormAdicionar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TxtBox_Marca = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtBox_Modelo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Combo_Tipo = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.BTN_GravarItem = new System.Windows.Forms.Button();
            this.BTN_CancelarItem = new System.Windows.Forms.Button();
            this.TxtBox_Quantidade = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // TxtBox_Marca
            // 
            this.TxtBox_Marca.Location = new System.Drawing.Point(87, 12);
            this.TxtBox_Marca.Name = "TxtBox_Marca";
            this.TxtBox_Marca.Size = new System.Drawing.Size(165, 23);
            this.TxtBox_Marca.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "Marca";
            // 
            // TxtBox_Modelo
            // 
            this.TxtBox_Modelo.Location = new System.Drawing.Point(87, 55);
            this.TxtBox_Modelo.Name = "TxtBox_Modelo";
            this.TxtBox_Modelo.Size = new System.Drawing.Size(165, 23);
            this.TxtBox_Modelo.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 15);
            this.label2.TabIndex = 3;
            this.label2.Text = "Modelo";
            // 
            // Combo_Tipo
            // 
            this.Combo_Tipo.FormattingEnabled = true;
            this.Combo_Tipo.Location = new System.Drawing.Point(87, 133);
            this.Combo_Tipo.Name = "Combo_Tipo";
            this.Combo_Tipo.Size = new System.Drawing.Size(121, 23);
            this.Combo_Tipo.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 141);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(30, 15);
            this.label3.TabIndex = 5;
            this.label3.Text = "Tipo";
            // 
            // BTN_GravarItem
            // 
            this.BTN_GravarItem.Location = new System.Drawing.Point(12, 216);
            this.BTN_GravarItem.Name = "BTN_GravarItem";
            this.BTN_GravarItem.Size = new System.Drawing.Size(75, 23);
            this.BTN_GravarItem.TabIndex = 6;
            this.BTN_GravarItem.Text = "Gravar";
            this.BTN_GravarItem.UseVisualStyleBackColor = true;
            this.BTN_GravarItem.Click += new System.EventHandler(this.BTN_GravarItem_Click);
            // 
            // BTN_CancelarItem
            // 
            this.BTN_CancelarItem.Location = new System.Drawing.Point(156, 216);
            this.BTN_CancelarItem.Name = "BTN_CancelarItem";
            this.BTN_CancelarItem.Size = new System.Drawing.Size(75, 23);
            this.BTN_CancelarItem.TabIndex = 7;
            this.BTN_CancelarItem.Text = "Cancelar";
            this.BTN_CancelarItem.UseVisualStyleBackColor = true;
            this.BTN_CancelarItem.Click += new System.EventHandler(this.BTN_CancelarItem_Click);
            // 
            // TxtBox_Quantidade
            // 
            this.TxtBox_Quantidade.Location = new System.Drawing.Point(87, 94);
            this.TxtBox_Quantidade.Name = "TxtBox_Quantidade";
            this.TxtBox_Quantidade.Size = new System.Drawing.Size(85, 23);
            this.TxtBox_Quantidade.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 97);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 15);
            this.label4.TabIndex = 9;
            this.label4.Text = "Quantidade";
            // 
            // FormAdicionar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(265, 257);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TxtBox_Quantidade);
            this.Controls.Add(this.BTN_CancelarItem);
            this.Controls.Add(this.BTN_GravarItem);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Combo_Tipo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.TxtBox_Modelo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TxtBox_Marca);
            this.Name = "FormAdicionar";
            this.Text = "Adicionar Item";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private TextBox TxtBox_Marca;
        private Label label1;
        private TextBox TxtBox_Modelo;
        private Label label2;
        private ComboBox Combo_Tipo;
        private Label label3;
        private Button BTN_GravarItem;
        private Button BTN_CancelarItem;
        private TextBox TxtBox_Quantidade;
        private Label label4;
    }
}