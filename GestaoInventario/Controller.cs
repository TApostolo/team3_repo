﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoInventario
{
    internal class Controller
    {
        Model model;
        ModelLog modelLog;
        View view;

        public Controller()
        {
            // instanciar o modelo e a view
            view = new View();
            model = new Model(view);

            view.Model = model;

            modelLog = new ModelLog();
            model.modelLog = modelLog;

            // subscrever eventos da View
            view.UtilizadorClicouEmListarInventario += UtilizadorClicouEmListaInventario;
            view.UtilizadorClicouEmSair += UtilizadorClicouEmSair;

            view.UtilizadorClicouEmAdicionarItem += UtilizadorClicouEmAdicionarItem;
            view.UtilizadorClicouEmCancelarAdicionarItem += UtilizadorClicouEmCancelarAdicionarItem;
            view.UtilizadorAdicionouItem += AdicionarItem;

            view.UtilizadorClicouEmAlterarItem += UtilizadorClicouEmAlterarItem;
            view.UtilizadorClicouEmCancelarAlterarItem += UtilizadorClicouEmCancelarAlterarItem;
            view.UtilizadorAlterouItem += AlterarItem;

            view.UtilizadorRemoveuItem += RemoverItem;
            view.SolicitarLog += modelLog.ObterLog;

            // subscrever eventos do Model
            model.ListaInventarioPronta += view.SolicitarListaInventario;
            model.ItemFoiAdicionado += ExibirResultadoItemAdicionado;
            model.ItemFoiAlterado += ExibirResultadoItemAlterado;
            model.ItemFoiRemovido += ExbibirResultadoItemRemovido;

            modelLog.NotificarLogAlterado += view.NotificacaoLogAlterado;


        }

        public void IniciarPrograma()
        {
            // ativar a view
            view.AtivarInterface();
        }

        public void UtilizadorClicouEmListaInventario(object? sender, EventArgs e)
        {
            try
            {
                model.ListarInventario();
            }
            catch (TipoDesconhecidoException ex)
            {
                TipoDesconhecido(ex.Message);
            }
        }
        public void UtilizadorClicouEmSair(object? sender, EventArgs e)
        {
            view.Sair();
        }

        #region AdicionarItem

        public void UtilizadorClicouEmAdicionarItem(object? sender, EventArgs e)
        {
            view.AtivarFormAdicionar();
            view.MostrarFormAdicionar();
        }

        public void UtilizadorClicouEmCancelarAdicionarItem(object? sender, EventArgs e)
        {
            view.FecharFormAdicionar();
        }

        public void AdicionarItem(Item itemAdicionado)
        {
            model.AdicionarItemInventario(itemAdicionado);
        }


        public void RemoverItem(int itemID)
        {
            model.RemoverItemInventario(itemID);
        }

        public void ExbibirResultadoItemRemovido()
        {
            view.ExibirResultadoItemRemovido();
        }

        public void ExibirResultadoItemAdicionado()
        {
            view.ExibirResultadoItemAdicionado();
        }
        #endregion

        #region AlterarItem
        public void UtilizadorClicouEmAlterarItem(int itemID)
        {
            Item item = model.ObterItemInventario(itemID);
            if (item != null)
                view.AtivarFormAlterar(item);

            view.MostrarFormAlterar();
        }

        public void UtilizadorClicouEmCancelarAlterarItem(object? sender, EventArgs e)
        {
            view.FecharFormAlterar();
        }

        public void ExibirResultadoItemAlterado()
        {
            view.ExibirResultadoItemAlterado();
        }

        public void AlterarItem(Item item)
        {
            model.AlterarItemInventario(item);
        }
        #endregion

        #region TratamentoErros
        private void TipoDesconhecido(string message)
        {
            view.AtivarViewLog();
            modelLog.RegistarLog($"Tipo do item desconhecido: {message}");
        }
        #endregion

    }
}
