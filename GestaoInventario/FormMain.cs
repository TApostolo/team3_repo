using System.Text;

namespace GestaoInventario
{
    public partial class FormMain : Form
    {
        public View? View { get; set; }

        public FormMain()
        {
            InitializeComponent();
            InicializarLista();
        }

        /// <summary>
        /// Carrega os dados na lista para exibi��o na aplica��o
        /// </summary>
        /// <param name="inventario"></param>
        public void DesenharListaInventario(List<Item> inventario)
        {
            // limpar a tabela antes de refrescar os dados
            listView_Dados.Items.Clear();

            // se estiver vazio, indicar 
            if (inventario?.Count > 0)
            {
                foreach (Item item in inventario)
                {
                    ListViewItem listItem = new ListViewItem(
                        new string[]
                        {
                            item.ID.ToString(),
                            item.Marca.ToString(),
                            item.Modelo.ToString(),
                            item.Tipo.ToString(),
                            item.Quantidade.ToString()
                        });

                    listView_Dados.Items.Add(listItem);
                }

            }
            else
            {
                // preparar uma linha vazia sen�o fica sem colunas e o AutoResizeColumn falha
                listView_Dados.Items.Add(new ListViewItem(new string[]{string.Empty, "Lista Vazia", string.Empty, string.Empty}));
            }
        }

        public void ExibirResultadoItemRemovido()
        {
            MessageBox.Show("Item removido com sucesso", "Remover Item", MessageBoxButtons.OK);
        }

        /// <summary>
        /// Termina a aplica��o
        /// </summary>
        public void Sair()
        {
            Application.Exit();
        }

        /// <summary>
        /// Exibe todos os itens no invent�rio
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Btn_ListarInventario_Click(object sender, EventArgs e)
        {
            View?.ListarInventario(sender, e);

            // mudar a coluna do tipo para o conte�do
            // uma vez que sei que este � sempre maior que o cabe�alho
            listView_Dados.AutoResizeColumn(3, ColumnHeaderAutoResizeStyle.ColumnContent);
        }

        private void BTN_Sair_Click(object sender, EventArgs e)
        {
            View?.ClicouEmSair(sender, e);
        }

        private void BTN_Adicionar_Click(object sender, EventArgs e)
        {
            View?.ClicouEmAdicionarItem(sender, e);
        }

        private void Btn_AlterarItem_Click(object sender, EventArgs e)
        {
            // obter o ID do item selecionado
            int itemID = ObterIDSelecionado();

            if (itemID > 0)
            {
                View?.ClicouEmAlterarItem(itemID);
            }
            else
            {
                // emitir um aviso de que apenas pode selecionar um item para ser alterado
                MessageBox.Show("Selecionar um item para ser alterado", "Aten��o", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void Btn_RemoverItem_Click(object sender, EventArgs e)
        {
            // obter o ID do item selecionado
            int itemID = ObterIDSelecionado();

            if (itemID > 0)
            {
                View?.ClicouEmRemoverItem(itemID);
            }
            else
            {
                // emitir um aviso de que apenas pode selecionar um item para ser removido
                MessageBox.Show("Selecionar um item para ser removido", "Aten��o", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        }

        /// <summary>
        /// Ajusta os tamanhos das colunas 
        /// </summary>
        private void InicializarLista()
        {
            listView_Dados.View = System.Windows.Forms.View.Details;
            listView_Dados.GridLines = true;
            listView_Dados.Columns.Add("ID");
            listView_Dados.Columns.Add("Marca");
            listView_Dados.Columns.Add("Modelo");
            listView_Dados.Columns.Add("Tipo");
            listView_Dados.Columns.Add("Quantidade");

            // esconder a coluna do ID
            listView_Dados.Columns[0].Width = 0;

            // outras colunas come�am com o tamanho do cabe�alho
            listView_Dados.AutoResizeColumn(1, ColumnHeaderAutoResizeStyle.HeaderSize);
            listView_Dados.AutoResizeColumn(2, ColumnHeaderAutoResizeStyle.HeaderSize);
            listView_Dados.AutoResizeColumn(3, ColumnHeaderAutoResizeStyle.HeaderSize);
            listView_Dados.AutoResizeColumn(4, ColumnHeaderAutoResizeStyle.HeaderSize);
        }

        /// <summary>
        /// Obt�m o identificador do item atualmente selecionado
        /// </summary>
        /// <returns></returns>
        private int ObterIDSelecionado()
        {
            int itemID = 0;
            // obter o ID do item selecionado
            var selected = listView_Dados.SelectedItems;

            if (selected.Count == 1)
            {
               itemID = int.Parse(selected[0].Text);
            }

            return itemID;
        }

    }
}