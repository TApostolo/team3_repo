﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoInventario
{
    public class ModelLog : ILog
    {
        public delegate void NotificacaoLogAlterado();
        public event NotificacaoLogAlterado? NotificarLogAlterado;

        private string? log;

        public ModelLog()
        {
            // limpar o log na incialização
            log = string.Empty;
        }

        public void RegistarLog(string log)
        {
            this.log = log;
            NotificarLogAlterado?.Invoke();
        }

        public string ObterLog()
        {
            if(log == null)
                log = string.Empty;

            return log;
        }
    }
}
