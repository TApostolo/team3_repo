﻿namespace GestaoInventario
{
    partial class FormLog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Txtbox_Log = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // Txtbox_Log
            // 
            this.Txtbox_Log.Location = new System.Drawing.Point(12, 12);
            this.Txtbox_Log.Multiline = true;
            this.Txtbox_Log.Name = "Txtbox_Log";
            this.Txtbox_Log.Size = new System.Drawing.Size(414, 290);
            this.Txtbox_Log.TabIndex = 0;
            // 
            // FormLog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(438, 314);
            this.Controls.Add(this.Txtbox_Log);
            this.Name = "FormLog";
            this.Text = "Log Aplicacional";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private TextBox Txtbox_Log;
    }
}