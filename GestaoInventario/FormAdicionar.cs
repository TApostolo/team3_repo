﻿namespace GestaoInventario
{
    public partial class FormAdicionar : Form
    {
        public ViewAdicionar? View { get; set; }

        public FormAdicionar()
        {
            InitializeComponent();
            Combo_Tipo.DataSource = Enum.GetValues(typeof(TipoItem));
            Combo_Tipo.DropDownStyle = ComboBoxStyle.DropDownList;
            Combo_Tipo.SelectedIndex = 0; // por omissão cadeira
        }

        public void ExibirResultadoItemAdicionado()
        {
            MessageBoxButtons botoes = MessageBoxButtons.OK;

            string mensagem = "Item adicionado com sucesso";

            DialogResult resultado = MessageBox.Show(mensagem, "Adicionar Item", botoes);

            if (resultado == DialogResult.OK)
            {
                this.Close();
            }
        }

        /// <summary>
        /// Limpa as caixas de texto e fecha o form
        /// </summary>
        public void FecharForm()
        {
            TxtBox_Marca.Text = string.Empty;
            TxtBox_Modelo.Text = string.Empty;

            this.Close();
        }

        private void BTN_GravarItem_Click(object sender, EventArgs e)
        {
            Item itemAdicionar = new Item
            {
                Marca = TxtBox_Marca.Text,
                Modelo = TxtBox_Modelo.Text,
                Quantidade = Convert.ToInt32(TxtBox_Quantidade.Text),
                Tipo = (TipoItem)Combo_Tipo.SelectedItem
            };

            View?.AdicionarItem(itemAdicionar);
        }

        /// <summary>
        /// Notifica que o utilizador quer cancelar a adição do item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BTN_CancelarItem_Click(object sender, EventArgs e)
        {
            View?.ClicouEmCancelar(sender, e);
        }
    }
}
