﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoInventario
{
    public class ViewLog : ILog
    {
        FormLog? formularioLog;
        Form pai;

        public ViewLog(Form origem)
        {
            if(pai == null)
                pai = origem;
        }

        public void AtivarInterface()
        {
            if (formularioLog == null)
                formularioLog = new FormLog();
        }

        /// <summary>
        /// Regista o texto no log e exibe o formulário
        /// </summary>
        /// <param name="log"></param>
        public void RegistarLog(string log)
        {
            // se o formulário de log não existir, sair
            if (formularioLog == null)
                throw new FormularioInexistenteException();
            
            formularioLog.RegistarLog(log);
            formularioLog.ShowDialog(pai);
        }
    }
}
