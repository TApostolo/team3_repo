﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoInventario
{
    public class View
    {
        public Model? Model { get; set; }
        private readonly ViewAdicionar viewAdicionarItem;
        private readonly ViewAlterar viewAlterarItem;
        private readonly ViewLog? viewLog;

        private FormMain? formularioPrincipal;
        private FormLog? formularioLog;

        // eventos
        public event EventHandler? UtilizadorClicouEmListarInventario;
        public event EventHandler? UtilizadorClicouEmSair;

        public event EventHandler? UtilizadorClicouEmAdicionarItem;
        public event EventHandler? UtilizadorClicouEmCancelarAdicionarItem;

        public event EventHandler? UtilizadorClicouEmCancelarAlterarItem;


        public delegate void NotificarUtilizadorAdicionouItem(Item itemAdicionado);
        public event NotificarUtilizadorAdicionouItem? UtilizadorAdicionouItem;

        public delegate void NotificarUtilizadorClicouEmAlterarItem(int itemID);
        public event NotificarUtilizadorClicouEmAlterarItem? UtilizadorClicouEmAlterarItem;

        public delegate void NotificarUtilizadorRemoveuItem(int itemID);
        public event NotificarUtilizadorRemoveuItem? UtilizadorRemoveuItem;

        public delegate void NotificarUtilizadorAlterouItem(Item item);
        public event NotificarUtilizadorAlterouItem? UtilizadorAlterouItem;

        public delegate string SolicitarLogRegistado();
        public event SolicitarLogRegistado? SolicitarLog;



        public View()
        {
            viewAdicionarItem = new ViewAdicionar(formularioPrincipal);
            viewAlterarItem = new ViewAlterar(formularioPrincipal);
            viewLog = new ViewLog(formularioLog);


            // subscrever eventos adicionar item
            viewAdicionarItem.UtilizadorClicouEmGravar += ItemAdicionado;
            viewAdicionarItem.UtilizadorClicouEmCancelar += ClicouEmCancelarAdicionaritem;

            // eventos alterar item
            viewAlterarItem.UtilizadorClicouEmGravar += ItemAlterado;
            viewAlterarItem.UtilizadorClicouEmCancelar += ClicouEmCancelarAlterarItem;
        }

        public View(Model m)
        {
            Model = m;
            viewAdicionarItem = new ViewAdicionar(formularioPrincipal);
        }

        public void AtivarInterface()
        {
            formularioPrincipal = new FormMain
            {
                View = this
            };
            formularioPrincipal.ShowDialog();
        }
        public void SolicitarListaInventario()
        {
            List<Item>? inventario = Model?.SolicitarListaInventario();
            formularioPrincipal?.DesenharListaInventario(inventario);
        }

        public void Sair()
        {
            formularioPrincipal?.Sair();
        }


        #region ViewAdicionar

        public void AtivarFormAdicionar()
        {
            viewAdicionarItem.AtivarInterface();
        }

        public void MostrarFormAdicionar()
        {
            viewAdicionarItem.ExibirFormAdicionar(formularioPrincipal);
        }

        public void FecharFormAdicionar()
        {
            viewAdicionarItem.FecharForm();
        }

        /// <summary>
        /// Invoca a messagebox para mostrar o resultado da operação
        /// </summary>
        public void ExibirResultadoItemAdicionado()
        {
            viewAdicionarItem.ExibirResultadoItemAdicionado();
        }

        public void ExibirResultadoItemRemovido()
        {
            formularioPrincipal?.ExibirResultadoItemRemovido();
        }

        #endregion

        #region ViewAlterar
        public void AtivarFormAlterar(Item item)
        {
            viewAlterarItem.AtivarInterface(item);
        }

        public void MostrarFormAlterar()
        {
            viewAlterarItem.ExibirFormAlterar(formularioPrincipal);
        }

        public void FecharFormAlterar()
        {
            viewAlterarItem.FecharForm();
        }

        public void ExibirResultadoItemAlterado()
        {
            viewAlterarItem.ExibirResultadoItemAlterado();
        }
        #endregion

        #region ViewLog
        public void AtivarViewLog()
        {
            viewLog?.AtivarInterface();
        }

        public void NotificacaoLogAlterado()
        {
            if (SolicitarLog != null)
                viewLog?.RegistarLog(SolicitarLog());
        }


        #endregion

        #region Eventos
        public void ListarInventario(object sender, EventArgs e)
        {
            UtilizadorClicouEmListarInventario?.Invoke(sender, e);
        }
        public void ClicouEmRemoverItem(int itemID)
        {
            UtilizadorRemoveuItem?.Invoke(itemID);
        }

        public void ClicouEmSair(object? sender, EventArgs e)
        {
            UtilizadorClicouEmSair?.Invoke(sender, e);
        }

        #region AdicionarItem
        /// <summary>
        /// Notifica o controller que o item foi adicionado pelo utilizador
        /// </summary>
        /// <param name="itemAdicionado"></param>
        public void ItemAdicionado(Item itemAdicionado)
        {
            UtilizadorAdicionouItem?.Invoke(itemAdicionado);
        }

        public void ClicouEmAdicionarItem(object sender, EventArgs e)
        {
            UtilizadorClicouEmAdicionarItem?.Invoke(sender, e);
        }

        public void ClicouEmCancelarAdicionaritem(object? sender, EventArgs e)
        {
            UtilizadorClicouEmCancelarAdicionarItem?.Invoke(sender, e);
        }
        #endregion

        #region AlterarItem
        public void ItemAlterado(Item item)
        {
            UtilizadorAlterouItem?.Invoke(item);
        }

        public void ClicouEmAlterarItem(int itemID)
        {
            UtilizadorClicouEmAlterarItem?.Invoke(itemID);
        }

        public void ClicouEmCancelarAlterarItem(object? sender, EventArgs e)
        {
            UtilizadorClicouEmCancelarAlterarItem?.Invoke(sender, e);
        }
        #endregion

        #endregion
    }
}
