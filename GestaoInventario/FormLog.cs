﻿namespace GestaoInventario
{
    public partial class FormLog : Form
    {
        public FormLog()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Acrescenta o texto de log à TextBox
        /// </summary>
        /// <param name="log"></param>
        public void RegistarLog(string log)
        {
            Txtbox_Log.Text += log;
        }
    }
}
