﻿using System.Runtime.Serialization;

namespace GestaoInventario
{
    public class TipoDesconhecidoException : Exception
    {
        public TipoDesconhecidoException()
        { 
        }

        public TipoDesconhecidoException(string message) : base(message)
        {

        }

        public TipoDesconhecidoException(string message, Exception innerException) : base(message, innerException)
        {

        }

        protected TipoDesconhecidoException(SerializationInfo info, StreamingContext context) : base(info, context)
        {

        }
    }
}
