﻿namespace GestaoInventario
{
    partial class FormMain
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_ListarInventario = new System.Windows.Forms.Button();
            this.BTN_Sair = new System.Windows.Forms.Button();
            this.BTN_Adicionar = new System.Windows.Forms.Button();
            this.listView_Dados = new System.Windows.Forms.ListView();
            this.Btn_RemoverItem = new System.Windows.Forms.Button();
            this.Btn_AlterarItem = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_ListarInventario
            // 
            this.btn_ListarInventario.Location = new System.Drawing.Point(12, 12);
            this.btn_ListarInventario.Name = "btn_ListarInventario";
            this.btn_ListarInventario.Size = new System.Drawing.Size(108, 32);
            this.btn_ListarInventario.TabIndex = 0;
            this.btn_ListarInventario.Text = "Listar Inventário";
            this.btn_ListarInventario.UseVisualStyleBackColor = true;
            this.btn_ListarInventario.Click += new System.EventHandler(this.Btn_ListarInventario_Click);
            // 
            // BTN_Sair
            // 
            this.BTN_Sair.Location = new System.Drawing.Point(12, 404);
            this.BTN_Sair.Name = "BTN_Sair";
            this.BTN_Sair.Size = new System.Drawing.Size(75, 23);
            this.BTN_Sair.TabIndex = 2;
            this.BTN_Sair.Text = "Sair";
            this.BTN_Sair.UseVisualStyleBackColor = true;
            this.BTN_Sair.Click += new System.EventHandler(this.BTN_Sair_Click);
            // 
            // BTN_Adicionar
            // 
            this.BTN_Adicionar.Location = new System.Drawing.Point(12, 50);
            this.BTN_Adicionar.Name = "BTN_Adicionar";
            this.BTN_Adicionar.Size = new System.Drawing.Size(108, 27);
            this.BTN_Adicionar.TabIndex = 3;
            this.BTN_Adicionar.Text = "Adicionar Item";
            this.BTN_Adicionar.UseVisualStyleBackColor = true;
            this.BTN_Adicionar.Click += new System.EventHandler(this.BTN_Adicionar_Click);
            // 
            // listView_Dados
            // 
            this.listView_Dados.FullRowSelect = true;
            this.listView_Dados.Location = new System.Drawing.Point(126, 12);
            this.listView_Dados.Name = "listView_Dados";
            this.listView_Dados.Size = new System.Drawing.Size(662, 426);
            this.listView_Dados.TabIndex = 4;
            this.listView_Dados.UseCompatibleStateImageBehavior = false;
            // 
            // Btn_RemoverItem
            // 
            this.Btn_RemoverItem.Location = new System.Drawing.Point(12, 140);
            this.Btn_RemoverItem.Name = "Btn_RemoverItem";
            this.Btn_RemoverItem.Size = new System.Drawing.Size(108, 27);
            this.Btn_RemoverItem.TabIndex = 5;
            this.Btn_RemoverItem.Text = "Remover Item";
            this.Btn_RemoverItem.UseVisualStyleBackColor = true;
            this.Btn_RemoverItem.Click += new System.EventHandler(this.Btn_RemoverItem_Click);
            // 
            // Btn_AlterarItem
            // 
            this.Btn_AlterarItem.Location = new System.Drawing.Point(12, 83);
            this.Btn_AlterarItem.Name = "Btn_AlterarItem";
            this.Btn_AlterarItem.Size = new System.Drawing.Size(108, 29);
            this.Btn_AlterarItem.TabIndex = 6;
            this.Btn_AlterarItem.Text = "Alterar Item";
            this.Btn_AlterarItem.UseVisualStyleBackColor = true;
            this.Btn_AlterarItem.Click += new System.EventHandler(this.Btn_AlterarItem_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Btn_AlterarItem);
            this.Controls.Add(this.Btn_RemoverItem);
            this.Controls.Add(this.listView_Dados);
            this.Controls.Add(this.BTN_Adicionar);
            this.Controls.Add(this.BTN_Sair);
            this.Controls.Add(this.btn_ListarInventario);
            this.Name = "FormMain";
            this.Text = "Gestão de Inventário";
            this.ResumeLayout(false);

        }

        #endregion

        private Button btn_ListarInventario;
        private Button BTN_Sair;
        private Button BTN_Adicionar;
        private ListView listView_Dados;
        private Button Btn_RemoverItem;
        private Button Btn_AlterarItem;
    }
}