﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using GestaoInventario.Interface;

namespace GestaoInventario.Database
{
    public class Datasource : IDatasource
    {
        SQLiteConnection m_database;
        private readonly string FicheiroBD = @"Database\inventario.db";

        #region Campos
        private const string ID = "ID";
        private const string MODELO = "Modelo";
        private const string MARCA = "Marca";
        private const string QUANTIDADE = "Quantidade";
        private const string TIPO_ITEM = "TipoItem";
        #endregion

        public Datasource()
        {
            // a base de dados está na diretoria Database logo abaixo da aplicação
            // assim obtenho essa diretoria a calculo onde está o ficheiro da bd
            string diretoriaExecucao = Directory.GetCurrentDirectory();

            m_database = new SQLiteConnection($"Data Source={Path.Combine(diretoriaExecucao, FicheiroBD)};Version=3;");
        }

        #region ADD
        public bool AdicionarItem(Item itemParaAdicionar)
        {
            int tipoItem = Convert.ToInt32(itemParaAdicionar.Tipo);

            m_database.Open();

            SQLiteCommand sqlCommand = new(
                $"INSERT INTO Item (Modelo, Marca, Quantidade, TipoItem) VALUES ('{itemParaAdicionar.Modelo}', '{itemParaAdicionar.Marca}', '{itemParaAdicionar.Quantidade}', '{tipoItem}')"
                , m_database);

            bool itemAdicionado = sqlCommand.ExecuteNonQuery() > 0;
            m_database.Close();

            return itemAdicionado;
        }
        #endregion

        #region GET
        public List<Item> ListarInventario()
        {
            List<Item> inventario = null;

            m_database.Open();

            SQLiteCommand listarInventario = new SQLiteCommand(
                $"SELECT * FROM ITEM"
                , m_database
                );

            SQLiteDataReader dataReader = listarInventario.ExecuteReader();

            while (dataReader.Read())
            {
                // inicializar a lista
                inventario = inventario ?? new List<Item>();

                Item item = new Item();

                int tempID = dataReader.GetOrdinal(ID);
                item.ID = dataReader.GetInt32(tempID);
                tempID = dataReader.GetOrdinal(MARCA);
                item.Marca = dataReader.GetString(tempID);
                tempID = dataReader.GetOrdinal(MODELO);
                item.Modelo = dataReader.GetString(tempID);
                tempID = dataReader.GetOrdinal(QUANTIDADE);
                item.Quantidade = dataReader.GetInt32(tempID);
                tempID = dataReader.GetOrdinal(TIPO_ITEM);
                item.Tipo = (TipoItem)dataReader.GetInt32(tempID);

                inventario.Add(item);
            }

            // destruir o leitor e fechar base de dados
            dataReader.DisposeAsync();
            m_database.Close();

            return inventario;
        }

        public Item ObterItem(int itemID)
        {
            Item item = null;

            m_database.Open();

            SQLiteCommand obterItem = new SQLiteCommand(
                $"SELECT * FROM Item WHERE ID = '{itemID}';"
                ,m_database);

            SQLiteDataReader dataReader = obterItem.ExecuteReader();

            if(dataReader.Read())
            {
                item = new Item();

                int tempID = dataReader.GetOrdinal(ID);
                item.ID = dataReader.GetInt32(tempID);
                tempID = dataReader.GetOrdinal(MARCA);
                item.Marca = dataReader.GetString(tempID);
                tempID = dataReader.GetOrdinal(MODELO);
                item.Modelo = dataReader.GetString(tempID);
                tempID = dataReader.GetOrdinal(QUANTIDADE);
                item.Quantidade = dataReader.GetInt32(tempID);
                tempID = dataReader.GetOrdinal(TIPO_ITEM);
                item.Tipo = (TipoItem)dataReader.GetInt32(tempID);
            }

            // destruir o leitor e fechar base de dados
            dataReader.DisposeAsync();
            m_database.Close();

            return item;
        }
        #endregion

        #region UPDATE
        public bool AlterarItem(int itemID, string modelo, string marca)
        {
            m_database.Open();

            SQLiteCommand alterarItem = new SQLiteCommand(
                $"UPDATE Item SET Modelo = '{modelo}', Marca = '{marca}' WHERE ID = '{itemID}';"
                , m_database);

            bool itemAlterado = alterarItem.ExecuteNonQuery() > 0;

            m_database.Close();
            return itemAlterado;
        }
        #endregion

        #region DELETE
        public bool RemoverItem(int itemID)
        {
            m_database.Open();

            SQLiteCommand removerItem = new SQLiteCommand(
                $"DELETE FROM Item WHERE ID = '{itemID}'"
                , m_database);

            bool itemRemovido = removerItem.ExecuteNonQuery() > 0;

            m_database.Close();
            return itemRemovido;
        }
        #endregion

    }
}
