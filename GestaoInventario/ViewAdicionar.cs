﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoInventario
{
    public class ViewAdicionar
    {
        private FormAdicionar? FormularioAdicionarItem;
        private FormMain? Pai;

        // eventos
        public delegate void NotificarUtilizadorAdicionouItem(Item itemAdicionado);
        public event NotificarUtilizadorAdicionouItem? UtilizadorClicouEmGravar;

        public event EventHandler? UtilizadorClicouEmCancelar;

        public ViewAdicionar(FormMain pai)
        {
            Pai = pai;
        }

        /// <summary>
        /// Apresenta o formulário para o utilizador adicionar o item
        /// </summary>
        public void AtivarInterface()
        {
            FormularioAdicionarItem = new FormAdicionar
            {
                View = this
            };
        }

        public void FecharForm()
        {
            FormularioAdicionarItem?.FecharForm();
        }

        public void ExibirFormAdicionar(FormMain origem)
        {
            if(Pai == null)
            {
                Pai = origem;
            }

            FormularioAdicionarItem?.ShowDialog(Pai);
        }

        public void ExibirResultadoItemAdicionado()
        {
            FormularioAdicionarItem?.ExibirResultadoItemAdicionado();
        }

        public void AdicionarItem(Item itemAdicionado)
        {
            UtilizadorClicouEmGravar?.Invoke(itemAdicionado);
        }


        public void ClicouEmCancelar(object sender, EventArgs e)
        {
            UtilizadorClicouEmCancelar?.Invoke(sender, e);
        }

    }
}
