﻿using GestaoInventario.Database;
using GestaoInventario.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoInventario
{
    public class Model
    {
        private View View;

        public Item? ItemAdicionado { get; set; }
        public ModelLog? modelLog { get; set; }

        IDatasource datasource;

        private List<Item> Inventario;

        // eventos

        public delegate void NotificarItemFoiAdicionado();
        public event NotificarItemFoiAdicionado? ItemFoiAdicionado;

        public delegate void NotificarItemFoiAlterado();
        public event NotificarItemFoiAlterado? ItemFoiAlterado;

        public delegate void NotificarListaInventarioPronta();
        public event NotificarListaInventarioPronta? ListaInventarioPronta;

        public delegate void NotificarItemInventarioRemovido();
        public event NotificarItemInventarioRemovido? ItemFoiRemovido;

        public Model(View view)
        {
            View = view;
            Inventario = new List<Item>();
            datasource = new Datasource();
        }

        /// <summary>
        /// Lista todos os items em inventário
        /// </summary>
        public void ListarInventario()
        {
            Inventario = datasource.ListarInventario();

            ListaInventarioPronta?.Invoke();
        }

        public List<Item> SolicitarListaInventario()
        {

            return Inventario;
        }

        /// <summary>
        /// Adiciona o item indicado ao inventário
        /// </summary>
        /// <param name="itemParaAdicionar"></param>
        public void AdicionarItemInventario(Item itemParaAdicionar)
        {
            if (datasource.AdicionarItem(itemParaAdicionar))
                ItemFoiAdicionado?.Invoke();
        }

        /// <summary>
        /// Altera a marca e o modelo do item indicado
        /// </summary>
        /// <param name="itemID"></param>
        public void AlterarItemInventario(Item item)
        {

            if (datasource.AlterarItem((int)item?.ID, item.Modelo, item.Marca))
                ItemFoiAlterado?.Invoke();

        }

        public Item ObterItemInventario(int itemID)
        {
            return datasource.ObterItem(itemID);
        }

        /// <summary>
        /// Remove o item correspondente ao id fornecido da lista
        /// </summary>
        /// <param name="itemID"></param>
        public void RemoverItemInventario(int itemID)
        {
            if (datasource.RemoverItem(itemID))
                ItemFoiRemovido?.Invoke();
        }
    }
}
