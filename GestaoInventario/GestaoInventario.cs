namespace GestaoInventario
{
    internal static class GestaoInventario
    {
        /// <summary>
        ///  Ponto de entrada para a aplica��o demonstradora
        /// </summary>
        [STAThread]
        static void Main()
        {
            Controller controller = new Controller();

            controller.IniciarPrograma();
        }
    }
}