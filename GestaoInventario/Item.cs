﻿namespace GestaoInventario
{
    public enum TipoItem
    {
        Cadeira = 1,
        Mesa,
        Computador,
        Candeeiro
    }
    /// <summary>
    /// Classe para os items a serem registados no inventário
    /// </summary>
    public class Item
    {
        public int? ID { get; set; }
        public int Quantidade { get; set; }
        public string? Marca { get; set; }
        public string? Modelo { get; set; }
        public TipoItem? Tipo { get; set; }

        
        /// <summary>
        /// Converte Item numa string para ser colocar na textbox
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return $"ID:{ID}\tMarca: {Marca}\tModelo: {Modelo}\tTipo: {Enum.GetName(typeof(TipoItem), Tipo)}";
        }
    }
}
