﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoInventario
{
    public interface ILog
    {
        /// <summary>
        /// Regista a mensagem em log
        /// </summary>
        /// <param name="log"></param>
        void RegistarLog(string log);
    }
}
